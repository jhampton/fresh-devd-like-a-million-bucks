#! /bin/sh

# Make sure we only run on Mac
case "$(uname)" in
  'Darwin' )
    ;;
  * )
    printf "This script can only run on a Mac\n"
    exit 1
    ;;
esac

# clone dotfiles repo
git clone https://github.com/johnrhampton/dotfiles.git $HOME/dotfiles

# clone toolbelt repo
git clone https://github.com/ChuckLangford/toolbelt.git $HOME/toolbelt

# Brew
printf "Do you want to install/update Brew? (y/n) "
read InstallBrew

if [[ $InstallBrew == 'y' ]]; then
  which brew > /dev/null
  if [[ $? != 0 ]]; then
    printf "Installing Brew\n"
    /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
  else
    printf "Updating Brew\n"
    brew update
  fi
fi

# Git
printf "Do you want to install/update git? (y/n) "
read InstallGit

if [[ $InstallGit == 'y' ]]; then
  brew info git | grep "Not Installed"
  if [[ $? == 0 ]]; then
    printf "Installing Git\n"
    brew install git
  else
    brew outdated | grep "git"
    if [[ $? == 0 ]]; then
      printf "Updating Git\n"
      brew update git
    else
      printf "Already up-to-date\n"
    fi

  fi
fi

# Install iTerm2
printf "Do you want to install/update iTerm2? (y/n) "
read InstalliTerm

if [[ $InstalliTerm == 'y' ]]; then
  brew cask info iterm2 | grep "Not Installed"
  if [[ $? == 0 ]]; then
    printf "Installing iTerm2\n"
    brew cask install iterm2
  else
    brew cask outdated | grep "iterm2"
    if [[ $? == 0 ]]; then
      printf "Updating iTerm2\n"
      brew cask upgrade iterm2
    else
      printf "Already up-to-date\n"
    fi

  fi
fi

# set symbolic links
ln -s $HOME/dotfiles/.vimrc ~/.vimrc
ln -s $HOME/dotfiles/.zshrc ~/.zshrc
ln -s $HOME/dotfiles/.tmux.conf ~/.tmux.conf
ln -s $HOME/dotfiles/.eslintrc ~/.eslintrc

# Install Neovim
bash $HOME/toolbelt/install_scripts/install_neovim_mac.sh

# python 3
# nvm --lts
# go
# https://github.com/nsf/gocode or https://github.com/mdempsky/gocode 
# iterm
# Install oh-my-zsh
# Install powerline fonts
# Install tmux
# Install eslint with Airbnb rules
# clone the dot files repo
# Maybe? Install youcompleteme along with jtern??
# Install htop
# Install httpie
# nvim
# https://github.com/junegunn/vim-plug

# At end of script, print a list of all changes
